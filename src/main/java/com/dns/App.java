package com.dns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages={"com.dns"})
@EnableAutoConfiguration
@EnableJpaRepositories(value={"com.dns"})
public class App
extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(App.class, (String[])args);
    }
}