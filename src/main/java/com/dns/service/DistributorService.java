/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Distributor
 *  com.dns.repository.DistributorRepository
 *  com.dns.service.dto.DistributorDTO
 *  org.slf4j.Logger
 *  org.slf4j.LoggerFactory
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.http.HttpStatus
 *  org.springframework.http.ResponseEntity
 *  org.springframework.stereotype.Service
 */
package com.dns.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.dns.domain.Distributor;
import com.dns.repository.DistributorRepository;
import com.dns.service.dto.DistributorDTO;
import com.dns.service.dto.DistributorFilterDetailResultDTO;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@Service
public class DistributorService {
	private Logger log = LoggerFactory.getLogger(DistributorService.class);
	@Autowired
	private DistributorRepository distributorRepository;
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static final String SELECT_DISTRIBUTOR_FILTER_DETAIL_QUERY = "SELECT  p.name, sum(dp.quantity), sum(dp.sold_amount)  FROM dnstest2.product  p, discounted_products dp, sale_record s where dp.product_id=p.id and s.distributor_id=? and dp.order_id=s.id and s.last_modified_date between ? and ? group by p.name ";

	@Transactional
	public ResponseEntity<Boolean> storeDistributor(DistributorDTO distributor) {
		Distributor dist = new Distributor(distributor.getPassword(), distributor.getName().toUpperCase(),
				distributor.getAddress().toUpperCase(), distributor.getMobile(), distributor.getEmail(),
				distributor.getState().toUpperCase(), distributor.getCity().toUpperCase());
		try {
			if (distributorRepository.findByEmail(distributor.getEmail()) != null) {
				return new ResponseEntity<Boolean>(false, HttpStatus.ALREADY_REPORTED);
			}
		} catch (Exception e) {
			log.error("Getting while adding Dsitributor", (Throwable) e);
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		try {
			dist = distributorRepository.save(dist);
			if (!inventoryService.assigneInventory(dist.getId()))
				return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
			log.info(dist.toString());
		} catch (Exception e) {
			log.error("Getting error while adding distributor", (Throwable) e);
			return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.ACCEPTED);
	}

	public Boolean changePassword(Long id, String currentPassword, String newPassword) {
		try {
			Distributor distributor = null;
			if (currentPassword != null)
				distributor = distributorRepository.findByIdAndCurrentPassword(id, currentPassword);
			else
				distributor = distributorRepository.findOne(id);
			if (distributor == null) {
				return false;
			}
			distributor.setPassword(newPassword);
			distributorRepository.save(distributor);
		} catch (Exception e) {
			log.error("Getting error while changing password", (Throwable) e);
			return false;
		}
		return true;
	}

	public Distributor getDistributorByCity(String city) {
		Distributor distributor = null;
		try {
			distributor = distributorRepository.findByCity(city);
		} catch (Exception e) {
			log.error("getting error while fetching city wise distributer", e);
		}
		return distributor;
	}

	public List<Distributor> fetchAllDistributor() {
		try {
			return distributorRepository.findAll();
		} catch (Exception e) {
			log.error("getting error while fetching all distributer", e);
			return null;
		}
	}

	public ResponseEntity<Boolean> updateDistributor(Distributor distributor) {
		try {
			log.info(distributor.toString());
			distributor = distributorRepository.save(distributor);
			log.info(distributor.toString());
		} catch (Exception e) {
			return new ResponseEntity<Boolean>(true, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
	}

	public List<DistributorFilterDetailResultDTO> getDetailDistributorFilterResult(Long id, String fromDate,
			String toDate) {
		List<DistributorFilterDetailResultDTO> detailList = null;
		try {
			StringBuilder query = new StringBuilder();
			query.append(
					"SELECT  p.name, sum(dp.quantity) as quantity, sum(dp.sold_amount) as soldAmount  FROM dnstest2.product  p, discounted_products dp, sale_record s where dp.product_id=p.id and s.distributor_id="
							+ id + " and dp.order_id=s.id and s.last_modified_date between'" + fromDate + "' and '"
							+ toDate + "' group by p.name");

			
			List<Map<String, Object>> data = jdbcTemplate.queryForList(query.toString());
			log.info(query.toString());
			log.info(data.toString());
			if (data != null) {
				detailList = new ArrayList<DistributorFilterDetailResultDTO>();
				for (int i = 0; i < data.size(); i++) {
					Map<String, Object> d = data.get(i);
					DistributorFilterDetailResultDTO distributor = new DistributorFilterDetailResultDTO();
					distributor.setName(d.get("name").toString());
					BigDecimal quantity = (BigDecimal) d.get("quantity");
					distributor.setQuantity(quantity.intValue());
					distributor.setSoldAmount((BigDecimal) d.get("soldAmount"));
					
					detailList.add(distributor);
				}
			}
		} catch (Exception e) {
			log.error("EXCEPTION found while getting distributor filter detail", e);
		}
		return detailList;
	}
}
