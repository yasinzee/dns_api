package com.dns.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dns.domain.Distributor;
import com.dns.domain.Inventory;
import com.dns.domain.InventoryTransaction;
import com.dns.domain.Product;
import com.dns.repository.DistributorRepository;
import com.dns.repository.InventoryRepository;
import com.dns.repository.InventoryTransactionRepository;
import com.dns.repository.ProductRepository;
import com.dns.service.dto.Constants;
import com.dns.service.dto.OrderItemsDTO;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@Service
public class InventoryService {
	private Logger log = LoggerFactory.getLogger(InventoryService.class);
	@Autowired
	private InventoryRepository inventoryRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private DistributorRepository distributorRepository;
	@Autowired
	private InventoryTransactionRepository inventoryTransactionRepository;

	public void updateInventory(OrderItemsDTO order, Long distributorId) {
		inventoryRepository.updateStock(order.getProductId(),
				Integer.valueOf(order.getSoldStock() + order.getCount()), distributorId);
	}

	public List<Inventory> getInventoryDetails(Long id) {
		return inventoryRepository.getInventoryDetails(id);
	}

	public boolean assigneInventory(long distributerId) {
		List<Product> allProducts = null;
		try {
			allProducts = productRepository.findAll();
			log.info(allProducts.toString());
		} catch (Exception e) {
			log.error("Getting error while fetching all products to assign inventory to distributor", e);
			return false;
		}
		Distributor distributor = null;
		try {
			distributor = distributorRepository.findOne(distributerId);
			log.info(distributor.toString());
		} catch (Exception e) {
			log.error("Getting error while fetching distributor detail to assign inventory to distributor", e);
			return false;
		}
		List<Inventory> inventoryList = new ArrayList<Inventory>();
		for (Product product : allProducts) {
			inventoryList.add(new Inventory(100, 0, product, distributor));
		}

		try {
			inventoryRepository.save(inventoryList);
		} catch (Exception e) {
			log.error("Getting error while storing inventory to distributor", e);
			return false;
		}
		return true;
	}

	public Boolean updateInventory(Long inventoryId, Integer stock, String action, Long adminId) {
		Inventory inventory;
		try {
			inventory = inventoryRepository.findOne(inventoryId);
		} catch (Exception e) {
			log.error("Getting error while updating inventory to distributor", e);
			return false;
		}
		if (action.equalsIgnoreCase(Constants.INVENTORY_ADD))
			inventory.setTotalStock(inventory.getTotalStock() + stock);
		else if (action.equalsIgnoreCase(Constants.INVENTORY_SUB))
			inventory.setTotalStock(inventory.getTotalStock() - stock);
		try {
			inventory = inventoryRepository.save(inventory);
			addToInventoryTransaction(inventory, stock, action, adminId);
		} catch (Exception e) {
			log.error("Getting error while updating inventory to distributor", e);
			return false;
		}
		return true;
	}

	private InventoryTransaction addToInventoryTransaction(Inventory inventory, Integer stock, String action,
			Long adminId) {
		Distributor admin = null;
		InventoryTransaction inventoryTransaction = new InventoryTransaction();
		try {
			admin = distributorRepository.findOne(adminId);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		if (null != admin) {
			inventoryTransaction.setDistributor(inventory.getDistributorId());
			inventoryTransaction.setProduct(inventory.getProductId());
			inventoryTransaction.setQuantity(stock);
			inventoryTransaction.setAdmin(admin);
			inventoryTransaction.setIs_add(action.equalsIgnoreCase(Constants.INVENTORY_ADD) ? true : false);
			inventoryTransaction.setIs_subtract(action.equalsIgnoreCase(Constants.INVENTORY_SUB) ? true : false);
			inventoryTransaction.setUpdatedTimestamp(Instant.now().toString());
			try {
				inventoryTransaction = inventoryTransactionRepository.save(inventoryTransaction);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return inventoryTransaction;

	}

	public List<InventoryTransaction> getAllInventoryTransactions(Long id) {
		return inventoryTransactionRepository.findByAdminId(id);
	}
}
