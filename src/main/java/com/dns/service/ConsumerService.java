/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Consumer
 *  com.dns.repository.ConsumerRepository
 *  org.slf4j.Logger
 *  org.slf4j.LoggerFactory
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.http.HttpStatus
 *  org.springframework.http.ResponseEntity
 *  org.springframework.stereotype.Service
 */
package com.dns.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.dns.domain.Consumer;
import com.dns.repository.ConsumerRepository;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@Service
public class ConsumerService {
    private Logger log = LoggerFactory.getLogger(ConsumerService.class);
    @Autowired
    private ConsumerRepository consumerRepository;

    public ResponseEntity<Boolean> addConsumer(Consumer consumer) {
        try {
            if (consumerRepository.findByMobile(consumer.getMobile()) != null) {
                return new ResponseEntity<Boolean>(false, HttpStatus.ALREADY_REPORTED);
            }
        }
        catch (Exception e) {
            log.error("Getting while adding consumer", (Throwable)e);
            return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
        }
        try {
        	consumer.setAddress(consumer.getAddress().toString());
        	consumer.setCity(consumer.getCity().toUpperCase());
        	consumer.setState(consumer.getState().toUpperCase());
        	
            Consumer c = consumerRepository.save(consumer);
            log.info(c.toString());
        }
        catch (Exception e) {
            log.error("Getting while adding consumer", (Throwable)e);
            return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Boolean>(true, HttpStatus.ACCEPTED);
    }

    public List<Consumer> fetchConsumerList(String state, String city) {
        List<Consumer> c = null;
        try {
            c = consumerRepository.findByStateAndCity(state, city);
            log.info(c.toString());
        }
        catch (Exception e) {
            log.error("Getting while adding consumer",e);
            return null;
        }
        return c;
    }
    
    public List<Consumer> fetchConsumerListByCity(String city) {
    	List<Consumer> c = null;
        try {
            c = consumerRepository.findByCityOrderByNameAsc(city);
            log.info(c.toString());
        }
        catch (Exception e) {
            log.error("Getting while adding consumer", e);
            return null;
        }
        return c;
    }

	public ResponseEntity<Boolean> updateConsumer(Consumer consumer) {
		try {
			consumer = consumerRepository.save(consumer);
			log.info(consumer.toString());
		} catch (Exception e) {
			return new ResponseEntity<Boolean>(true, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);
	}
}
