/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Consumer
 *  com.dns.domain.DiscountedProducts
 *  com.dns.domain.Distributor
 *  com.dns.domain.OrderedProducts
 *  com.dns.domain.Product
 *  com.dns.domain.SaleRecord
 *  com.dns.repository.ConsumerRepository
 *  com.dns.repository.DiscountedProductsRepository
 *  com.dns.repository.DistributorRepository
 *  com.dns.repository.OrderedProductsRepository
 *  com.dns.repository.SaleRecordRepository
 *  com.dns.service.InventoryService
 *  com.dns.service.dto.OrderDetailsDTO
 *  com.dns.service.dto.OrderDetailsWithDisountedProductsDTO
 *  com.dns.service.dto.OrderItemsDTO
 *  com.dns.service.dto.SmsResponseStatusDTO
 *  com.dns.service.dto.TransactionalSmsBodyDTO
 *  com.dns.util.SmsService
 *  javax.transaction.Transactional
 *  org.slf4j.Logger
 *  org.slf4j.LoggerFactory
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.stereotype.Service
 */
package com.dns.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dns.domain.Consumer;
import com.dns.domain.DiscountedProducts;
import com.dns.domain.Distributor;
import com.dns.domain.OrderedProducts;
import com.dns.domain.SaleRecord;
import com.dns.repository.ConsumerRepository;
import com.dns.repository.DiscountedProductsRepository;
import com.dns.repository.DistributorRepository;
import com.dns.repository.OrderedProductsRepository;
import com.dns.repository.SaleRecordRepository;
import com.dns.service.dto.OrderDetailsDTO;
import com.dns.service.dto.OrderDetailsWithDisountedProductsDTO;
import com.dns.service.dto.OrderItemsDTO;
import com.dns.service.dto.SmsResponseStatusDTO;
import com.dns.service.dto.TransactionalSmsBodyDTO;
import com.dns.util.SmsService;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@Service
public class OrderService {
	private Logger log = LoggerFactory.getLogger(OrderService.class);
	@Autowired
	private DistributorRepository distributorRepository;
	@Autowired
	private SaleRecordRepository saleRecordRepository;
	@Autowired
	private ConsumerRepository consumerRepository;
	@Autowired
	private OrderedProductsRepository orderedProductsRepository;
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private DiscountedProductsRepository discountedProductsRepository;

	@Transactional
	public SaleRecord addOrders(OrderDetailsDTO orderDetails) {
		SaleRecord saleRecord=null;
		Consumer consumer;
		Distributor distributor = distributorRepository.findOne(orderDetails.getDistributorId());
		log.info("consumer name: " + orderDetails.getConsumerId());
		consumer = consumerRepository.findOne(orderDetails.getConsumerId());
		log.info("consumer: " + consumer);
		if (distributor != null && consumer != null) {
			saleRecord = new SaleRecord();
			saleRecord.setAmount(orderDetails.getTotalPrice());
			saleRecord.setConsumer(consumer);
			saleRecord.setDistributor(distributor);
			saleRecord.setCity(consumer.getCity());
			saleRecord.setCreatedDate(LocalDate.now().toString());
			saleRecord.setLastModifiedDate(LocalDate.now().toString());
			saleRecord.setState(consumer.getState());
			if (orderDetails.getAmountDues().equals(BigDecimal.ZERO)) {
				saleRecord.setStatus("PAID");
			} else {
				saleRecord.setStatus("DUES");
			}
			saleRecord.setAmountPaid(orderDetails.getAmountPaid());
			saleRecord.setAmountDues(orderDetails.getAmountDues());
			saleRecord =  saleRecordRepository.save(saleRecord);
			for (OrderItemsDTO order : orderDetails.getProductList()) {
				if (order.getCount() == 0)
					continue;
				try {
					log.info(order.toString());
					orderedProductsRepository.save(new OrderedProducts(order.getProductId(), saleRecord,
							order.getCount(), order.getTotalPrice()));
					inventoryService.updateInventory(order, orderDetails.getDistributorId());
				} catch (Exception e) {
					log.error("Getting error while storing ordered product", e);
				}
			}
			saveDiscountedProducts(orderDetails, saleRecord);
		}
		try {
			sendOrderConfirmationSms(saleRecord, consumer.getMobile());
		} catch (Exception e) {
			log.error("Getting Error while storing order details", e);
		}
		return saleRecord;
	}

	@Transactional
	private void saveDiscountedProducts(OrderDetailsDTO orderDetails, SaleRecord saleRecord) {
		for (OrderItemsDTO order : orderDetails.getFreebieProductList()) {
			if (order.getCount() == 0)
				continue;
			try {
				log.info(order.toString());
				discountedProductsRepository.save(new DiscountedProducts(order.getProductId(), saleRecord,
						order.getCount(), order.getTotalPrice()));
				inventoryService.updateInventory(order, orderDetails.getDistributorId());
			} catch (Exception e) {
				log.error("Getting error while storing ordered product", e);
			}
		}
	}

	private void sendOrderConfirmationSms(SaleRecord saleRecord, String mobile) {
		SmsResponseStatusDTO response = null;
		try {
			response = SmsService.sendSms(
					(TransactionalSmsBodyDTO) new TransactionalSmsBodyDTO("dnsdns", mobile, "dns_order_confirmation",
							saleRecord.getAmount().toString(), saleRecord.getAmountPaid().toString()));
			if (response.getStatus().equalsIgnoreCase("Success")) {
				log.info("Order confirmation message sent successfully");
			} else {
				log.info("Order confirmation message sending failed");
			}
		} catch (Exception e) {
			log.error("Getting error while sending order confirmation sms", e);
		}
	}

	public List<SaleRecord> fetchAllPaidRecords(Long id) {
		List<SaleRecord> saleRecordList = null;
		try {
			saleRecordList = saleRecordRepository.findPaidByDistributorId(id);
		} catch (Exception e) {
			log.error("Getting error while fetching all order details", e);
			return null;
		}
		return saleRecordList;
	}

	public List<SaleRecord> fetchAllDuesRecords(Long id) {
		List<SaleRecord> saleRecordList = null;
		try {
			saleRecordList = saleRecordRepository.findDuesByDistributorId(id);
		} catch (Exception e) {
			log.error("Getting error while fetching all order details", e);
			return null;
		}
		return saleRecordList;
	}

	public OrderDetailsWithDisountedProductsDTO fetchProductByOrder(Long id) {
		List<OrderedProducts> orderedProductList = null;
		List<DiscountedProducts> discountedProductsList = null;
		try {
			orderedProductList = orderedProductsRepository.findByOrderId(id);
		} catch (Exception e) {
			log.error("Getting error while fetching all order details", e);
			orderedProductList = null;
		}
		try {
			discountedProductsList = discountedProductsRepository.findByOrderId(id);
		} catch (Exception e) {
			log.error("Getting error while fetching discounted order details", e);
			discountedProductsList = null;
		}
		return new OrderDetailsWithDisountedProductsDTO(orderedProductList, discountedProductsList);
	}

	public Boolean settleDues(Long id, BigDecimal duesAmount) {
		try {
			SaleRecord order = (SaleRecord) saleRecordRepository.findOne(id);
			order.setAmountDues(order.getAmountDues().subtract(duesAmount));
			order.setAmountPaid(order.getAmountPaid().add(duesAmount));
			order.setLastModifiedDate(LocalDate.now().toString());
			if (order.getAmountDues().equals(BigDecimal.ZERO)) {
				order.setStatus("PAID");
			}
			saleRecordRepository.save(order);
		} catch (Exception e) {
			log.error("Getting error while settling up dues", e);
			return false;
		}
		return true;
	}

	public BigDecimal getDuesAmountByConsumer(Long id) {
		try {
			return saleRecordRepository.findDuesAmountByConsumer(id);
		} catch (Exception e) {
			log.error("Getting Error while fetching total dues amount", e);
			return null;
		}
	}
}
