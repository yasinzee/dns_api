/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Product
 */
package com.dns.service.dto;

import com.dns.domain.Product;
import java.math.BigDecimal;

public class OrderItemsDTO {
    private Long id;
    private Product productId;
    private Integer totalStock;
    private Integer soldStock;
    private Integer count;
    private BigDecimal totalPrice;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProductId() {
        return this.productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public Integer getTotalStock() {
        return this.totalStock;
    }

    public void setTotalStock(Integer totalStock) {
        this.totalStock = totalStock;
    }

    public Integer getSoldStock() {
        return this.soldStock;
    }

    public void setSoldStock(Integer soldStock) {
        this.soldStock = soldStock;
    }

    public Integer getCount() {
        return this.count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String toString() {
        return "OrderItemsDTO [id=" + this.id + ", productId=" + this.productId + ", totalStock=" + this.totalStock + ", soldStock=" + this.soldStock + ", count=" + this.count + ", totalPrice=" + this.totalPrice + "]";
    }
}
