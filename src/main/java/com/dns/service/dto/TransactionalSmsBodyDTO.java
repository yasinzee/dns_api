/*
 * Decompiled with CFR 0_132.
 */
package com.dns.service.dto;

public class TransactionalSmsBodyDTO {
    private String From;
    private String To;
    private String TemplateName;
    private String VAR1;
    private String VAR2;

    public TransactionalSmsBodyDTO() {
    }

    public TransactionalSmsBodyDTO(String from, String to, String templateName, String vAR1, String vAR2) {
        this.From = from;
        this.To = to;
        this.TemplateName = templateName;
        this.VAR1 = vAR1;
        this.VAR2 = vAR2;
    }

    public String getFrom() {
        return this.From;
    }

    public void setFrom(String from) {
        this.From = from;
    }

    public String getTo() {
        return this.To;
    }

    public void setTo(String to) {
        this.To = to;
    }

    public String getTemplateName() {
        return this.TemplateName;
    }

    public void setTemplateName(String templateName) {
        this.TemplateName = templateName;
    }

    public String getVAR1() {
        return this.VAR1;
    }

    public void setVAR1(String vAR1) {
        this.VAR1 = vAR1;
    }

    public String getVAR2() {
        return this.VAR2;
    }

    public void setVAR2(String vAR2) {
        this.VAR2 = vAR2;
    }

    public String toString() {
        return "TransactionalSmsBodyDTO [From=" + this.From + ", To=" + this.To + ", TemplateName=" + this.TemplateName + ", VAR1=" + this.VAR1 + ", VAR2=" + this.VAR2 + "]";
    }
}
