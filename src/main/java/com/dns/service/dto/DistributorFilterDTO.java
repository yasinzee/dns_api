package com.dns.service.dto;

import java.math.BigDecimal;

import com.dns.domain.Distributor;

public class DistributorFilterDTO {
	private Distributor distributor;
	private BigDecimal totalAmount;
	
	public DistributorFilterDTO(Distributor distributor, BigDecimal totalAmount) {
		super();
		this.distributor = distributor;
		this.totalAmount = totalAmount;
	}

	public DistributorFilterDTO() {
		super();
	}

	public Distributor getDistributor() {
		return distributor;
	}

	public void setDistributor(Distributor distributor) {
		this.distributor = distributor;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	
	
}
