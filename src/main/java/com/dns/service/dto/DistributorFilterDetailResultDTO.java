package com.dns.service.dto;

import java.math.BigDecimal;

public class DistributorFilterDetailResultDTO {

	private String name;
	private int quantity;
	private BigDecimal soldAmount;
	
	public DistributorFilterDetailResultDTO(String name, int quantity, BigDecimal soldAmount) {
		super();
		this.name = name;
		this.quantity = quantity;
		this.soldAmount = soldAmount;
	}
	
	public DistributorFilterDetailResultDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getSoldAmount() {
		return soldAmount;
	}
	public void setSoldAmount(BigDecimal soldAmount) {
		this.soldAmount = soldAmount;
	}   
}
