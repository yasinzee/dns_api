/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  javax.persistence.Column
 */
package com.dns.service.dto;

import javax.persistence.Column;

public class DistributorDTO {
    @Column(name="password", nullable=false, length=255)
    private String password;
    @Column(name="name", nullable=false, length=255)
    private String name;
    @Column(name="address", nullable=false, length=1024)
    private String address;
    @Column(name="mobile", nullable=false, length=10)
    private String mobile;
    @Column(name="email", nullable=false, length=55)
    private String email;
    @Column(name="city", nullable=false, length=55)
    private String city;
    @Column(name="state", nullable=false, length=55)
    private String state;

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String toString() {
        return "DistributorDTO [password=" + this.password + ", name=" + this.name + ", address=" + this.address + ", mobile=" + this.mobile + ", email=" + this.email + ", city=" + this.city + ", state=" + this.state + "]";
    }
}
