/*
 * Decompiled with CFR 0_132.
 */
package com.dns.service.dto;

public class Constants {
    public static final String ORDER_STATUS_PAID = "PAID";
    public static final String ORDER_STATUS_DUES = "DUES";
    public static final String SMS_API = "5465ee4f-c55a-11e8-a895-0200cd936042";
    public static final String ORDER_CONFIRMED_SENDER_ID = "dnsdns";
    public static final String ORDER_CONFIRMED_TEMPLATE_NAME = "dns_order_confirmation";
    public static final String INVENTORY_ADD = "ADD";
    public static final String INVENTORY_SUB = "SUB";
    public static final String RETURN_REASON = "NO REASON";
}
