/*
 * Decompiled with CFR 0_132.
 */
package com.dns.service.dto;

public class SmsResponseStatusDTO {
    private String Status;
    private String Details;

    public SmsResponseStatusDTO(String status, String details) {
        this.Status = status;
        this.Details = details;
    }

    public SmsResponseStatusDTO() {
    }

    public String getStatus() {
        return this.Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getDetails() {
        return this.Details;
    }

    public void setDetails(String details) {
        this.Details = details;
    }

    public String toString() {
        return "OtpStatusDTO [Status=" + this.Status + ", Details=" + this.Details + "]";
    }
}
