/*
 * Decompiled with CFR 0_132.
 */
package com.dns.service.dto;

import com.dns.domain.Consumer;
import com.dns.domain.Distributor;
import com.dns.service.dto.OrderItemsDTO;
import java.math.BigDecimal;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class ConsumerListAlongWithDistributorDTO {
    private List<Consumer> consumerList;
    private Distributor distributor;
    
	public ConsumerListAlongWithDistributorDTO(List<Consumer> consumerList, Distributor distributor) {
		super();
		this.consumerList = consumerList;
		this.distributor = distributor;
	}
	public ConsumerListAlongWithDistributorDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public List<Consumer> getConsumerList() {
		return consumerList;
	}
	public void setConsumerList(List<Consumer> consumerList) {
		this.consumerList = consumerList;
	}
	public Distributor getDistributor() {
		return distributor;
	}
	public void setDistributor(Distributor distributor) {
		this.distributor = distributor;
	}
    
    
}
