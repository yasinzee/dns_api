package com.dns.service.dto;

import com.dns.service.dto.OrderItemsDTO;
import java.math.BigDecimal;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class OrderDetailsDTO {
    private List<OrderItemsDTO> productList;
    private List<OrderItemsDTO> freebieProductList;
    private BigDecimal totalPrice;
    private BigDecimal amountPaid;
    private BigDecimal amountDues;
    private Long consumerId;
    private Long distributorId;

    public List<OrderItemsDTO> getProductList() {
        return this.productList;
    }

    public void setProductList(List<OrderItemsDTO> productList) {
        this.productList = productList;
    }

    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getConsumerId() {
        return this.consumerId;
    }

    public void setConsumerId(Long consumerId) {
        this.consumerId = consumerId;
    }

    public Long getDistributorId() {
        return this.distributorId;
    }

    public void setDistributorId(Long distributorId) {
        this.distributorId = distributorId;
    }

    public BigDecimal getAmountPaid() {
        return this.amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public BigDecimal getAmountDues() {
        return this.amountDues;
    }

    public void setAmountDues(BigDecimal amountDues) {
        this.amountDues = amountDues;
    }

    public List<OrderItemsDTO> getFreebieProductList() {
        return this.freebieProductList;
    }

    public void setFreebieProductList(List<OrderItemsDTO> freebieProductList) {
        this.freebieProductList = freebieProductList;
    }
}
