package com.dns.service.dto;

import java.math.BigDecimal;

import com.dns.domain.Consumer;

public class ConsumerFilterDTO {
	private Consumer consumer;
	private BigDecimal totalAmount;
	
	public ConsumerFilterDTO() {
		super();
	}
	public ConsumerFilterDTO(Consumer consumer, BigDecimal totalAmount) {
		super();
		this.consumer = consumer;
		this.totalAmount = totalAmount;
	}
	public Consumer getConsumer() {
		return consumer;
	}
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
}
