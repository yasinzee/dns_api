package com.dns.service.dto;

public class FilterDetailsDTO {

	private String fromDate;
	private String toDate;
	private String city;
	private Long distributor;
	private Long consumer;
	private Long product;
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Long getDistributor() {
		return distributor;
	}
	public void setDistributor(Long distributor) {
		this.distributor = distributor;
	}
	public Long getConsumer() {
		return consumer;
	}
	public void setConsumer(Long consumer) {
		this.consumer = consumer;
	}
	public Long getProduct() {
		return product;
	}
	public void setProduct(Long product) {
		this.product = product;
	}
}
