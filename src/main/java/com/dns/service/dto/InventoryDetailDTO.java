/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Product
 */
package com.dns.service.dto;

import com.dns.domain.Product;

public class InventoryDetailDTO {
    Product product;
    private Integer totalStock;
    private Integer soldStock;
}
