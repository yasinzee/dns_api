package com.dns.service.dto;

import java.math.BigDecimal;

import com.dns.domain.OrderedProducts;
import com.dns.domain.Product;

public class ProductFilterDTO {
	private Product product;
	private Integer quantity;
	private BigDecimal totalAmount;
	
	public ProductFilterDTO() {
		super();
	}
	public ProductFilterDTO(Product product, Integer quantity,
			BigDecimal totalAmount) {
		super();
		this.product = product;
		this.quantity = quantity;
		this.totalAmount = totalAmount;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
}
