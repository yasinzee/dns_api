package com.dns.service.dto;

import com.dns.domain.DiscountedProducts;
import com.dns.domain.OrderedProducts;
import java.util.List;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class OrderDetailsWithDisountedProductsDTO {
    private List<OrderedProducts> orderedProductList;
    private List<DiscountedProducts> discountedProductsList;

    public OrderDetailsWithDisountedProductsDTO(List<OrderedProducts> orderedProductList, List<DiscountedProducts> discountedProductsList) {
        this.orderedProductList = orderedProductList;
        this.discountedProductsList = discountedProductsList;
    }

    public OrderDetailsWithDisountedProductsDTO() {
    }

    public List<OrderedProducts> getOrderedProductList() {
        return this.orderedProductList;
    }

    public void setOrderedProductList(List<OrderedProducts> orderedProductList) {
        this.orderedProductList = orderedProductList;
    }

    public List<DiscountedProducts> getDiscountedProductsList() {
        return this.discountedProductsList;
    }

    public void setDiscountedProductsList(List<DiscountedProducts> discountedProductsList) {
        this.discountedProductsList = discountedProductsList;
    }
}
