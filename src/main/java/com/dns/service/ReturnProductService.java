package com.dns.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dns.domain.OrderedProducts;
import com.dns.domain.ReturnProducts;
import com.dns.domain.SaleRecord;
import com.dns.repository.OrderedProductsRepository;
import com.dns.repository.ReturnProductRepository;
import com.dns.repository.SaleRecordRepository;
import com.dns.service.dto.Constants;
import com.dns.service.dto.OrderDetailsDTO;
import com.dns.service.dto.OrderItemsDTO;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@Service
public class ReturnProductService {
    private Logger log = LoggerFactory.getLogger(ReturnProductService.class);
    @Autowired
    private SaleRecordRepository saleRecordRepository;
    @Autowired
    private ReturnProductRepository returnProductRepository;
    @Autowired
    private OrderedProductsRepository orderedProductsRepository;

    @Transactional
    public Boolean processReturnRequest(OrderDetailsDTO orderDetails, Long orderId) {
        SaleRecord order = null;
        try {
            order = saleRecordRepository.findOne(orderId);
        }
        catch (Exception e) {
            log.error("Getting error while retrieving order in processing return order",e);
            return false;
        }
        ArrayList<ReturnProducts> returnProductList = new ArrayList<ReturnProducts>();
        for (OrderItemsDTO orderItem : orderDetails.getProductList()) {
            if (orderItem.getCount() <= 0) 
            	continue;
            
            returnProductList.add(new ReturnProducts(orderItem.getProductId(), order, orderItem.getCount(), orderItem.getTotalPrice(), Constants.RETURN_REASON, LocalDate.now().toString()));
        }
        try {
            log.info(returnProductList.toString());
            returnProductList = (ArrayList<ReturnProducts>) returnProductRepository.save(returnProductList);
            if (!updateOrderHistory(returnProductList)) {
                return false;
            }
        }
        catch (Exception e) {
            log.info("Getting error while saving Return Request", (Throwable)e);
            return false;
        }
        return true;
    }

    @Transactional
    private boolean updateOrderHistory(List<ReturnProducts> returnProductList) {
        try {
            for (ReturnProducts product : returnProductList) {
                OrderedProducts orderedProduct = orderedProductsRepository.findByOrderAndProductId(product.getOrder().getId(), product.getProduct().getId());
                orderedProduct.setQuantity(Integer.valueOf(orderedProduct.getQuantity() - product.getQuantity()));
                orderedProductsRepository.save(orderedProduct);
            }
        }
        catch (Exception e) {
            log.error("Getting error while updating ordered product history", (Throwable)e);
            return false;
        }
        return true;
    }

    public List<ReturnProducts> getReturnedProductByOrderId(Long orderId) {
        List<ReturnProducts> returnProductList = null;
        try {
            returnProductList = returnProductRepository.getReturnProductsByOrderId(orderId);
        }
        catch (Exception e) {
            log.info("Getting error while retrieving return products", (Throwable)e);
        }
        return returnProductList;
    }
}
