package com.dns.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.dns.domain.Consumer;
import com.dns.domain.Distributor;
import com.dns.domain.Product;
import com.dns.service.dto.ConsumerFilterDTO;
import com.dns.service.dto.DistributorFilterDTO;
import com.dns.service.dto.FilterDetailsDTO;
import com.dns.service.dto.ProductFilterDTO;

@Service
public class FilterService {

	@Autowired
	private JdbcTemplate jdbc;
	private Logger log = LoggerFactory.getLogger(FilterService.class);

	public List<ProductFilterDTO> getProductFilterResults(FilterDetailsDTO filterDetails) {
		List<ProductFilterDTO> productFilterList = null;
		try {

			StringBuilder query = new StringBuilder();
			query.append(
					"SELECT p.id, p.name, p.type, p.price, p.description, p.weight, sum(op.quantity) as quantity, sum(op.sold_amount) as soldAmount");
			query.append(" FROM product p");
			query.append(" JOIN ordered_products op on op.product_id=p.id ");
			query.append(" JOIN sale_record sale ON op.order_id=sale.id and op.quantity>0 ");

			if (filterDetails.getFromDate() != null && filterDetails.getToDate() != null) {
				query.append(" and sale.created_date between '" + filterDetails.getFromDate() + "' and '"
						+ filterDetails.getToDate() + "'");
			}

			if (filterDetails.getCity() != null) {
				query.append(" and sale.city='" + filterDetails.getCity() + "'");
			}

			if (filterDetails.getConsumer() != null) {
				query.append(" and sale.consumer_id=" + filterDetails.getConsumer());
			}

			if (filterDetails.getDistributor() != null) {
				query.append(" and sale.distributor_id=" + filterDetails.getDistributor());
			}

			if (filterDetails.getProduct() != null) {
				query.append(" and p.id=" + filterDetails.getProduct());
			}
			
			query.append(" group by p.name ");
			
			List<Map<String, Object>> data = jdbc.queryForList(query.toString());
			log.info(query.toString());
			log.info(data.toString());

			if (data != null) {
				productFilterList = new ArrayList<ProductFilterDTO>();
				for (int i = 0; i < data.size(); i++) {
					Map<String, Object> d = data.get(i);
					Product p = new Product(Long.parseLong(d.get("id").toString()), d.get("name").toString(),
							d.get("type").toString(), new BigDecimal(d.get("price").toString()),
							d.get("description").toString(), null);
					ProductFilterDTO productFilterDTO = new ProductFilterDTO(p,
							Integer.parseInt(d.get("quantity").toString()),
							new BigDecimal(d.get("soldAmount").toString()));
					productFilterList.add(productFilterDTO);

				}
			}
		} catch (Exception e) {
			log.error("EXCEPTION found while getting Filter data", e);
			return null;
		}

		return productFilterList;
	}

	// SELECT d.id, d.name, d.address, d.city, d.state, sum(s.amount) FROM
	// dnstest2.distributor d join sale_record s on d.id=s.distributor_id group
	// by d.id
	public List<DistributorFilterDTO> getDistributorFilterResults(FilterDetailsDTO filterDetails) {
		List<DistributorFilterDTO> distributorFilterDTOs = null;
		try {

			StringBuilder query = new StringBuilder();
			query.append("SELECT d.id, d.name, d.address, d.city, d.state, sum(sale.amount) as soldAmount");
			query.append(" FROM distributor d");
			query.append(" JOIN sale_record sale ON d.id=sale.distributor_id ");

			if (filterDetails.getFromDate() != null && filterDetails.getToDate() != null) {
				query.append(" and sale.created_date between '" + filterDetails.getFromDate() + "' and '"
						+ filterDetails.getToDate() + "'");
			}

			if (filterDetails.getCity() != null) {
				query.append(" and sale.city='" + filterDetails.getCity() + "'");
			}

			if (filterDetails.getConsumer() != null) {
				query.append(" and sale.consumer_id=" + filterDetails.getConsumer());
			}

			if (filterDetails.getDistributor() != null) {
				query.append(" and sale.distributor_id=" + filterDetails.getDistributor());
			}
			
			if (filterDetails.getProduct() != null) {
				query.append(" JOIN ordered_products op on op.order_id=sale.id and op.quantity>0");
				query.append(" and op.product_id=" + filterDetails.getProduct());
			}

			query.append(" group by d.id ");
			
			List<Map<String, Object>> data = jdbc.queryForList(query.toString());
			log.info(query.toString());
			log.info(data.toString());

			if (data != null) {
				distributorFilterDTOs = new ArrayList<DistributorFilterDTO>();
				for (int i = 0; i < data.size(); i++) {
					Map<String, Object> d = data.get(i);
					Distributor distributor = new Distributor(Long.parseLong(d.get("id").toString()),
							d.get("name").toString(), d.get("address").toString(), d.get("city").toString(),
							d.get("state").toString());
					
					DistributorFilterDTO distributorFilterDTO = new DistributorFilterDTO(distributor,
							new BigDecimal(d.get("soldAmount").toString()));
					distributorFilterDTOs.add(distributorFilterDTO);

				}
			}
		} catch (Exception e) {
			log.error("EXCEPTION found while getting filtering on the basis of Consumer", e);
			return null;
		}

		return distributorFilterDTOs;
	}

	
	//SELECT c.*, sum(s.amount) FROM dnstest2.consumer c join sale_record s on c.id=s.consumer_id and s.created_date between  '2017-07-08' and '2018-11-11' group by c.id
	public List<ConsumerFilterDTO> getConsumerFilterResults(FilterDetailsDTO filterDetails) {
		List<ConsumerFilterDTO> consumerFilterDTOs = null;
		try {

			StringBuilder query = new StringBuilder();
			query.append("SELECT c.*, sum(sale.amount) as soldAmount");
			query.append(" FROM consumer c");
			query.append(" JOIN sale_record sale ON c.id=sale.consumer_id ");

			if (filterDetails.getFromDate() != null && filterDetails.getToDate() != null) {
				query.append(" and sale.created_date between '" + filterDetails.getFromDate() + "' and '"
						+ filterDetails.getToDate() + "'");
			}

			if (filterDetails.getCity() != null) {
				query.append(" and sale.city='" + filterDetails.getCity() + "'");
			}

			if (filterDetails.getConsumer() != null) {
				query.append(" and sale.consumer_id=" + filterDetails.getConsumer());
			}

			if (filterDetails.getDistributor() != null) {
				query.append(" and sale.distributor_id=" + filterDetails.getDistributor());
			}
			
			if (filterDetails.getProduct() != null) {
				query.append(" JOIN ordered_products op on op.order_id=sale.id and op.quantity>0");
				query.append(" and op.product_id=" + filterDetails.getProduct());
			}

			query.append(" group by c.id ");
			
			List<Map<String, Object>> data = jdbc.queryForList(query.toString());
			log.info(query.toString());
			log.info(data.toString());

			if (data != null) {
				consumerFilterDTOs = new ArrayList<ConsumerFilterDTO>();
				for (int i = 0; i < data.size(); i++) {
					Map<String, Object> d = data.get(i);
					Consumer consumer = new Consumer(Long.parseLong(d.get("id").toString()),
							d.get("name").toString(), d.get("address").toString(), d.get("mobile").toString(), d.get("email").toString(), d.get("city").toString(),
							d.get("state").toString());
					
					ConsumerFilterDTO consumerFilterDTO = new ConsumerFilterDTO(consumer,
							new BigDecimal(d.get("soldAmount").toString()));
					consumerFilterDTOs.add(consumerFilterDTO);
				}
			}
		} catch (Exception e) {
			log.error("EXCEPTION found while getting filtering on the basis of Consumer", e);
			return null;
		}
		return consumerFilterDTOs;
	}	
}
