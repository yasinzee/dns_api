/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Product
 *  com.fasterxml.jackson.annotation.JsonIgnore
 *  javax.persistence.Column
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.JoinColumn
 *  javax.persistence.ManyToOne
 *  javax.persistence.Table
 */
package com.dns.domain;

import com.dns.domain.Distributor;
import com.dns.domain.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inventory")
public class Inventory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "total_stock", nullable = false, length = 10)
	private Integer totalStock;
	
	@Column(name = "sold_stock", nullable = false, length = 10)
	private Integer soldStock;
	
	@ManyToOne
	@JoinColumn(name = "product_id", nullable = false)
	private Product productId;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "distributor_id", nullable = false)
	private Distributor distributorId;

	public Inventory() {
		super();
	}

	public Inventory(Integer totalStock, Integer soldStock, Product productId, Distributor distributorId) {
		super();
		this.totalStock = totalStock;
		this.soldStock = soldStock;
		this.productId = productId;
		this.distributorId = distributorId;
	}

	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTotalStock() {
		return this.totalStock;
	}

	public void setTotalStock(Integer totalStock) {
		this.totalStock = totalStock;
	}

	public Integer getSoldStock() {
		return this.soldStock;
	}

	public void setSoldStock(Integer soldStock) {
		this.soldStock = soldStock;
	}

	public Product getProductId() {
		return this.productId;
	}

	public void setProductId(Product productId) {
		this.productId = productId;
	}

	public Distributor getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(Distributor distributorId) {
		this.distributorId = distributorId;
	}
	
}
