/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonIgnore
 *  javax.persistence.Column
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.JoinColumn
 *  javax.persistence.ManyToOne
 *  javax.persistence.Table
 */
package com.dns.domain;

import com.dns.domain.Consumer;
import com.dns.domain.Distributor;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="sale_record")
public class SaleRecord {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(name="status", nullable=false, length=55)
    private String status;
    @ManyToOne
    @JoinColumn(name="distributor_id", nullable=false)
    @JsonIgnore
    private Distributor distributor;
    @ManyToOne
    @JoinColumn(name="consumer_id", nullable=false)
    private Consumer consumer;
    @Column(name="amount", nullable=false, precision=12, scale=2)
    private BigDecimal amount;
    @Column(name="created_date", nullable=false)
    private String createdDate;
    @Column(name="last_modified_date", nullable=false)
    private String lastModifiedDate;
    @Column(name="city", nullable=false, length=55)
    private String city;
    @Column(name="state", nullable=false, length=55)
    private String state;
    @Column(name="amount_dues", precision=12, scale=2)
    private BigDecimal amountDues;
    @Column(name="amount_paid", precision=12, scale=2)
    private BigDecimal amountPaid;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Distributor getDistributor() {
        return this.distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public Consumer getConsumer() {
        return this.consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public BigDecimal getAmountDues() {
        return this.amountDues;
    }

    public void setAmountDues(BigDecimal amountDues) {
        this.amountDues = amountDues;
    }

    public BigDecimal getAmountPaid() {
        return this.amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String toString() {
        return "SaleRecord [id=" + this.id + ", status=" + this.status + ", amount=" + this.amount + ", createdDate=" + this.createdDate + ", lastModifiedDate=" + this.lastModifiedDate + ", city=" + this.city + ", state=" + this.state + ", amountDues=" + this.amountDues + ", amountPaid=" + this.amountPaid + "]";
    }
}
