/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Inventory
 *  com.fasterxml.jackson.annotation.JsonIgnore
 *  javax.persistence.CascadeType
 *  javax.persistence.Column
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.OneToMany
 *  javax.persistence.Table
 */
package com.dns.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@Entity
@Table(name = "distributor")
public class Distributor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "password", nullable = false, length = 255)
	private String password;

	@Column(name = "name", nullable = false, length = 255)
	private String name;
	
	@Column(name = "address", nullable = false, length = 1024)
	private String address;
	
	@Column(name = "mobile", nullable = false, length = 10)
	private String mobile;
	
	@Column(name = "email", nullable = false, length = 55)
	private String email;
	
	@Column(name = "city", nullable = false, length = 55)
	private String city;
	
	@Column(name = "state", nullable = false, length = 55)
	private String state;
	
	@OneToMany(mappedBy = "distributorId")
	@JsonIgnore
	private Set<Inventory> inventoryOfDistributor = new HashSet<Inventory>();

	public Distributor(String password, String name, String address, String mobile, String email, String city,
			String state) {
		this.password = password;
		this.name = name;
		this.address = address;
		this.mobile = mobile;
		this.email = email;
		this.city = city;
		this.state = state;
	}

	public Distributor(Long id, String name, String address, String city, String state) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.city = city;
		this.state = state;
	}


	public Distributor() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Set<Inventory> getInventoryOfDistributor() {
		return this.inventoryOfDistributor;
	}

	public void setInventoryOfDistributor(Set<Inventory> inventoryOfDistributor) {
		this.inventoryOfDistributor = inventoryOfDistributor;
	}

	public String toString() {
		return "Distributor [id=" + this.id + ", password=" + this.password + ", name=" + this.name + ", address="
				+ this.address + ", mobile=" + this.mobile + ", email=" + this.email + ", city=" + this.city
				+ ", state=" + this.state + "]";
	}
}
