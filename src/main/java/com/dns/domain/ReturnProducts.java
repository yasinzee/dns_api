/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Product
 *  com.dns.domain.SaleRecord
 *  javax.persistence.Column
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.JoinColumn
 *  javax.persistence.ManyToOne
 *  javax.persistence.Table
 */
package com.dns.domain;

import com.dns.domain.Product;
import com.dns.domain.SaleRecord;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="return_products")
public class ReturnProducts {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product product;
    @ManyToOne
    @JoinColumn(name="order_id", nullable=false)
    private SaleRecord order;
    @Column(name="quantity", nullable=false)
    private Integer quantity;
    @Column(name="amount", nullable=false)
    private BigDecimal amount;
    @Column(name="reason", nullable=false)
    private String reason;
    @Column(name="returned_date", nullable=false)
    private String returnedDate;

    public ReturnProducts(Product product, SaleRecord order, Integer quantity, BigDecimal amount, String reason, String returnedDate) {
        this.product = product;
        this.order = order;
        this.quantity = quantity;
        this.amount = amount;
        this.reason = reason;
        this.returnedDate = returnedDate;
    }

    public ReturnProducts() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SaleRecord getOrder() {
        return this.order;
    }

    public void setOrder(SaleRecord order) {
        this.order = order;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReturnedDate() {
        return this.returnedDate;
    }

    public void setReturnedDate(String returnedDate) {
        this.returnedDate = returnedDate;
    }

    public String toString() {
        return "ReturnProducts [id=" + this.id + ", product=" + this.product + ", order=" + this.order + ", quantity=" + this.quantity + ", amount=" + this.amount + ", reason=" + this.reason + ", returnedDate=" + this.returnedDate + "]";
    }
}
