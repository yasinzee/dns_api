/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  javax.persistence.Column
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.Table
 */
package com.dns.domain;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(name="name", nullable=false, length=255)
    private String name;
    @Column(name="type", nullable=false, length=255)
    private String type;
    @Column(name="price", nullable=false, precision=12, scale=2)
    private BigDecimal price;
    @Column(name="description", nullable=false, length=255)
    private String description;
    @Column(name="weight", nullable=false)
    private Integer weight;

    public Product() {
		super();
	}

	public Product(Long id, String name, String type, BigDecimal price, String description, Integer weight) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.price = price;
		this.description = description;
		this.weight = weight;
	}

	public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return this.description;
    }

    public Integer getWeight() {
        return this.weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return "Product [id=" + this.id + ", name=" + this.name + ", type=" + this.type + ", price=" + this.price + ", description=" + this.description + ", weight=" + this.weight + "]";
    }
}
