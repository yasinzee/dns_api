/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Product
 *  com.dns.domain.SaleRecord
 *  com.fasterxml.jackson.annotation.JsonIgnore
 *  javax.persistence.Column
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.JoinColumn
 *  javax.persistence.ManyToOne
 *  javax.persistence.Table
 */
package com.dns.domain;

import com.dns.domain.Product;
import com.dns.domain.SaleRecord;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="discounted_products")
public class DiscountedProducts {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product productId;
    @ManyToOne
    @JoinColumn(name="order_id", nullable=false)
    @JsonIgnore
    private SaleRecord order;
    @Column(name="quantity", nullable=false)
    private Integer quantity;
    @Column(name="sold_amount", nullable=false)
    private BigDecimal soldAmount;

    public DiscountedProducts() {
    }

    public DiscountedProducts(Product productId, SaleRecord order, Integer quantity, BigDecimal soldAmount) {
        this.productId = productId;
        this.order = order;
        this.quantity = quantity;
        this.soldAmount = soldAmount;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProductId() {
        return this.productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public SaleRecord getOrder() {
        return this.order;
    }

    public void setOrder(SaleRecord order) {
        this.order = order;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getSoldAmount() {
        return this.soldAmount;
    }

    public void setSoldAmount(BigDecimal soldAmount) {
        this.soldAmount = soldAmount;
    }
}
