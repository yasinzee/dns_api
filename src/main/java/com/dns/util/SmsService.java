/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.service.dto.SmsResponseStatusDTO
 *  com.dns.service.dto.TransactionalSmsBodyDTO
 *  org.slf4j.Logger
 *  org.slf4j.LoggerFactory
 *  org.springframework.http.HttpEntity
 *  org.springframework.http.HttpHeaders
 *  org.springframework.http.MediaType
 *  org.springframework.http.ResponseEntity
 *  org.springframework.util.MultiValueMap
 *  org.springframework.web.client.RestTemplate
 */
package com.dns.util;

import com.dns.service.dto.SmsResponseStatusDTO;
import com.dns.service.dto.TransactionalSmsBodyDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class SmsService {
    private static Logger log = LoggerFactory.getLogger(SmsService.class);

    public static SmsResponseStatusDTO sendSms(TransactionalSmsBodyDTO messageBody) {
        String url = "http://2factor.in/API/V1/5465ee4f-c55a-11e8-a895-0200cd936042/ADDON_SERVICES/SEND/TSMS";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity request = new HttpEntity(messageBody, (MultiValueMap)headers);
        messageBody = new TransactionalSmsBodyDTO("dnsdns", "8507476205", "dns_order_confirmation", "20000", "20000");
        log.info(messageBody.toString());
        ResponseEntity response = restTemplate.postForEntity(url, request, SmsResponseStatusDTO.class, new Object[0]);
        log.info(((SmsResponseStatusDTO)response.getBody()).toString());
        return (SmsResponseStatusDTO)response.getBody();
    }
}
