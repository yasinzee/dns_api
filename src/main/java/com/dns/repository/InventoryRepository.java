/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Inventory
 *  com.dns.domain.Product
 *  org.springframework.data.jpa.repository.JpaRepository
 *  org.springframework.data.jpa.repository.Modifying
 *  org.springframework.data.jpa.repository.Query
 *  org.springframework.stereotype.Repository
 */
package com.dns.repository;

import com.dns.domain.Inventory;
import com.dns.domain.Product;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository
extends JpaRepository<Inventory, Long> {
    @Query(value="select inventory from Inventory inventory where inventory.distributorId.id=?1")
    public List<Inventory> getInventoryDetails(Long var1);

    @Modifying
    @Query(value="update Inventory i set i.soldStock = ?2 where i.productId=?1 and i.distributorId.id=?3")
    public int updateStock(Product var1, Integer var2, Long var3);
}
