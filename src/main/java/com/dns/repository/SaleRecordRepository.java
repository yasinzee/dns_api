/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.SaleRecord
 *  org.springframework.data.jpa.repository.JpaRepository
 *  org.springframework.data.jpa.repository.Query
 *  org.springframework.stereotype.Repository
 */
package com.dns.repository;

import com.dns.domain.SaleRecord;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleRecordRepository
extends JpaRepository<SaleRecord, Long> {
    @Query(value="select salerecord from SaleRecord salerecord where salerecord.distributor.id=?1 and salerecord.status='PAID' order by salerecord.lastModifiedDate desc")
    public List<SaleRecord> findPaidByDistributorId(Long var1);

    @Query(value="select salerecord from SaleRecord salerecord where salerecord.distributor.id=?1 and salerecord.status='DUES' order by salerecord.lastModifiedDate desc")
    public List<SaleRecord> findDuesByDistributorId(Long var1);

    @Query(value="SELECT sum(sale.amountDues) FROM SaleRecord sale where sale.consumer.id=?1")
    public BigDecimal findDuesAmountByConsumer(Long var1);
}
