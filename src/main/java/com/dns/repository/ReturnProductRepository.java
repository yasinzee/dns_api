/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.ReturnProducts
 *  org.springframework.data.jpa.repository.JpaRepository
 *  org.springframework.data.jpa.repository.Query
 *  org.springframework.stereotype.Repository
 */
package com.dns.repository;

import com.dns.domain.ReturnProducts;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ReturnProductRepository
extends JpaRepository<ReturnProducts, Long> {
    @Query(value="select returnProducts from ReturnProducts returnProducts where returnProducts.order.id=?1")
    public List<ReturnProducts> getReturnProductsByOrderId(Long var1);
}
