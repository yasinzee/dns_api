/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Inventory
 *  com.dns.domain.Product
 *  org.springframework.data.jpa.repository.JpaRepository
 *  org.springframework.data.jpa.repository.Modifying
 *  org.springframework.data.jpa.repository.Query
 *  org.springframework.stereotype.Repository
 */
package com.dns.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.dns.domain.InventoryTransaction;

@Repository
public interface InventoryTransactionRepository
extends JpaRepository<InventoryTransaction, Long> {

	@Query("select transaction from InventoryTransaction transaction where transaction.admin.id=?1")
	List<InventoryTransaction> findByAdminId(Long id);
    
}
