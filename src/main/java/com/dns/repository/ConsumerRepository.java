/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Consumer
 *  org.springframework.data.jpa.repository.JpaRepository
 *  org.springframework.data.jpa.repository.Query
 *  org.springframework.stereotype.Repository
 */
package com.dns.repository;

import com.dns.domain.Consumer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsumerRepository
extends JpaRepository<Consumer, Long> {
    @Query(value="select consumer from Consumer consumer where consumer.name=?1")
    public Consumer findByName(String var1);

    public List<Consumer> findByCityOrderByNameAsc(String city);
    
    @Query(value="select consumer from Consumer consumer where consumer.state=?1 and consumer.city=?2")
    public List<Consumer> findByStateAndCity(String var1, String var2);

    public Consumer findByEmail(String var1);

    public Consumer findByMobile(String var1);
}
