package com.dns.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dns.domain.Product;

@Repository
public interface ProductRepository  extends JpaRepository<Product, Long>   {

}
