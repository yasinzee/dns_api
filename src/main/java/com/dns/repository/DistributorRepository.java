/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Distributor
 *  org.springframework.data.jpa.repository.JpaRepository
 *  org.springframework.data.jpa.repository.Query
 *  org.springframework.stereotype.Repository
 */
package com.dns.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.dns.domain.Distributor;
import com.dns.service.dto.DistributorFilterDetailResultDTO;

@Repository
public interface DistributorRepository
extends JpaRepository<Distributor, Long> {
    @Query(value="select distributor from Distributor distributor where distributor.email=?1 and distributor.password=?2")
    public Distributor authenticate(String var1, String var2);

    public Distributor findByEmail(String var1);
    
    public Distributor findByCity(String city);

    @Query(value="select distributor from Distributor distributor where distributor.id=?1 and distributor.password=?2")
    public Distributor findByIdAndCurrentPassword(Long var1, String var2);
    
    @Query(value="select distinct distributor.city from Distributor distributor")
    public List<String> getCities();
    
//    @Query("SELECT new com.dns.service.dto.DistributorFilterDetailResultDTO(p.name, sum(dp.quantity), sum(dp.soldAmount)) FROM Product p, "
//    		+ " DiscountedProducts dp, saleRecord s where dp.productId.id=p.id "
//    		+ " and s.distributor.id = ?1 and dp.order.id = s.id"
//    		+ " group by p.name")//s.lastModifiedDate between ?2 and ?3 
//    public List<DistributorFilterDetailResultDTO> getDistributorDetailFilter(Long distributorId, String fromDate, String toDate);
//    
}
