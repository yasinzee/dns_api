/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.OrderedProducts
 *  org.springframework.data.jpa.repository.JpaRepository
 *  org.springframework.data.jpa.repository.Query
 */
package com.dns.repository;

import com.dns.domain.OrderedProducts;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderedProductsRepository
extends JpaRepository<OrderedProducts, Long> {
    @Query(value="select product from OrderedProducts product where product.order.id=?1 and product.quantity>0")
    public List<OrderedProducts> findByOrderId(Long var1);

    @Query(value="select product from OrderedProducts product where product.order.id=?1 and product.productId.id=?2")
    public OrderedProducts findByOrderAndProductId(Long var1, Long var2);
}
