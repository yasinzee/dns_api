/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.DiscountedProducts
 *  org.springframework.data.jpa.repository.JpaRepository
 *  org.springframework.data.jpa.repository.Query
 */
package com.dns.repository;

import com.dns.domain.DiscountedProducts;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DiscountedProductsRepository
extends JpaRepository<DiscountedProducts, Long> {
    @Query(value="select product from DiscountedProducts product where product.order.id=?1 and product.quantity>0")
    public List<DiscountedProducts> findByOrderId(Long var1);

    @Query(value="select product from DiscountedProducts product where product.order.id=?1 and product.productId.id=?2")
    public DiscountedProducts findByOrderAndProductId(Long var1, Long var2);
}
