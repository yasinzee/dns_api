/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Consumer
 *  com.dns.resource.ProductResource
 *  com.dns.service.ConsumerService
 *  org.apache.log4j.Logger
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.http.HttpStatus
 *  org.springframework.http.ResponseEntity
 *  org.springframework.web.bind.annotation.GetMapping
 *  org.springframework.web.bind.annotation.PostMapping
 *  org.springframework.web.bind.annotation.RequestBody
 *  org.springframework.web.bind.annotation.RequestMapping
 *  org.springframework.web.bind.annotation.RequestParam
 *  org.springframework.web.bind.annotation.RestController
 */
package com.dns.resource;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dns.domain.Consumer;
import com.dns.service.ConsumerService;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@RestController
@RequestMapping(value={"/consumer"})
public class ConsumerResource {
    private static Logger log = Logger.getLogger(ProductResource.class);
    @Autowired
    private ConsumerService consumerService;

    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/add"})
    public ResponseEntity<Boolean> addConsumer(@RequestBody Consumer consumer) {
        return consumerService.addConsumer(consumer);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/fetch"})
    public ResponseEntity<List<Consumer>> getConsumerList(@RequestParam(name="state") String state, @RequestParam(name="city") String city) {
        return new ResponseEntity<List<Consumer>>(consumerService.fetchConsumerList(state, city), HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/city_wise_consumer"})
    public ResponseEntity<List<Consumer>> getCityWiseConsumerList(@RequestParam(name="city") String city) {
    	List<Consumer> consumerList=null;
		try {
			consumerList = consumerService.fetchConsumerListByCity(city);
		} catch (Exception e) {
			log.error("Getting error while retrieving cities list",e );
		}
    	return new ResponseEntity<List<Consumer>>(consumerList, HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/update"})
    public ResponseEntity<Boolean> updateConsumer(@RequestBody Consumer consumer) {
        log.info(consumer);
        return consumerService.updateConsumer(consumer);
    }
}
