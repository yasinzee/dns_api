/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Distributor
 *  com.dns.repository.DistributorRepository
 *  com.dns.service.dto.SmsResponseStatusDTO
 *  com.dns.service.dto.TransactionalSmsBodyDTO
 *  com.dns.util.SmsService
 *  org.apache.log4j.Logger
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.http.HttpStatus
 *  org.springframework.http.ResponseEntity
 *  org.springframework.web.bind.annotation.PostMapping
 *  org.springframework.web.bind.annotation.RequestBody
 *  org.springframework.web.bind.annotation.RequestMapping
 *  org.springframework.web.bind.annotation.RequestParam
 *  org.springframework.web.bind.annotation.RestController
 */
package com.dns.resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dns.domain.Distributor;
import com.dns.repository.DistributorRepository;
import com.dns.service.dto.SmsResponseStatusDTO;
import com.dns.service.dto.TransactionalSmsBodyDTO;
import com.dns.util.SmsService;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@RestController
@RequestMapping(value={"/login"})
public class LoginResource {
    @Autowired
    DistributorRepository distributorRepository;
    private static Logger log = Logger.getLogger(LoginResource.class);

    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/authenticate"})
    public ResponseEntity<Distributor> authenticate(@RequestParam(name="username") String username, @RequestParam(name="password") String password) {
        log.info(("Authenticating: " + username + " " + password));
        Distributor dist = distributorRepository.authenticate(username, password);
        log.info(("Authenticating: " + username + " " + password));
        log.info(dist);
        if (dist != null) {
            return new ResponseEntity<Distributor>(dist, HttpStatus.OK);
        }
        return new ResponseEntity<Distributor>(dist, HttpStatus.FORBIDDEN);
    }

//    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/sms"})
    public ResponseEntity<SmsResponseStatusDTO> sendSms(@RequestBody TransactionalSmsBodyDTO body) {
        SmsResponseStatusDTO response = null;
        try {
            response = SmsService.sendSms((TransactionalSmsBodyDTO)body);
        }
        catch (Exception e) {
            log.error("Getting error while sending sms", (Throwable)e);
        }
        return new ResponseEntity<SmsResponseStatusDTO>(response, HttpStatus.OK);
    }
    
    @GetMapping(value={"/hello"})
    public ResponseEntity<Boolean> hello() {
        log.info("Hello");
        return new ResponseEntity<Boolean>(true, HttpStatus.OK);
    }
}
