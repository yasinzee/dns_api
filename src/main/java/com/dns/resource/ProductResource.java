/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Product
 *  com.dns.repository.ProductRepository
 *  org.apache.log4j.Logger
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.http.HttpStatus
 *  org.springframework.http.ResponseEntity
 *  org.springframework.web.bind.annotation.PostMapping
 *  org.springframework.web.bind.annotation.RequestBody
 *  org.springframework.web.bind.annotation.RequestMapping
 *  org.springframework.web.bind.annotation.RestController
 */
package com.dns.resource;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dns.domain.Product;
import com.dns.repository.ProductRepository;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@RestController
@RequestMapping(value={"/product"})
public class ProductResource {
    private static Logger log = Logger.getLogger(ProductResource.class);
    @Autowired
    ProductRepository productRepository;

    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/add"})
    public ResponseEntity<String> addProduct(@RequestBody Product product) {
        log.info(product);
        productRepository.save(product);
        return new ResponseEntity<String>("Resource called", HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/all"})
    public ResponseEntity<List<Product>> GetAlProducts() {
    	List<Product> productList = null;
    	try {
			productList = productRepository.findAll();
		} catch (Exception e) {
			log.error("Getting error while fetch complete product list",e );
		}
    	
        return new ResponseEntity<List<Product>>(productList, HttpStatus.OK);
    }
}
