package com.dns.resource;

import com.dns.domain.ReturnProducts;
import com.dns.service.ReturnProductService;
import com.dns.service.dto.OrderDetailsDTO;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@RestController
@RequestMapping(value={"/order"})
public class ReturnProductResource {
    private static Logger log = Logger.getLogger(ReturnProductResource.class);
    @Autowired
    private ReturnProductService returnservice;

    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/return"})
    public ResponseEntity<Boolean> insertOrders(@RequestBody OrderDetailsDTO orderDetails, @RequestParam(name="id") Long orderId) {
        Boolean status = returnservice.processReturnRequest(orderDetails, orderId);
        return new ResponseEntity<Boolean>(status, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/return/fetch"})
    public ResponseEntity<List<ReturnProducts>> getReturnedProductByOrderId(@RequestParam(name="id") Long orderId) {
        return new ResponseEntity<List<ReturnProducts>>(returnservice.getReturnedProductByOrderId(orderId), HttpStatus.OK);
    }
}
