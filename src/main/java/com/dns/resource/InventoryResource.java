/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Inventory
 *  com.dns.service.InventoryService
 *  org.apache.log4j.Logger
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.http.HttpStatus
 *  org.springframework.http.ResponseEntity
 *  org.springframework.web.bind.annotation.GetMapping
 *  org.springframework.web.bind.annotation.RequestMapping
 *  org.springframework.web.bind.annotation.RequestParam
 *  org.springframework.web.bind.annotation.RestController
 */
package com.dns.resource;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dns.domain.Inventory;
import com.dns.domain.InventoryTransaction;
import com.dns.service.InventoryService;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@RestController
@RequestMapping(value={"/inventory"})
public class InventoryResource {
    private static Logger log = Logger.getLogger(InventoryResource.class);
    @Autowired
    private InventoryService inventoryService;

    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/fetch"})
    public ResponseEntity<List<Inventory>> getInventoryProducts(@RequestParam(name="id") Long id) {
        List<Inventory> inventoryList = inventoryService.getInventoryDetails(id);
        log.info(inventoryList);
        return new ResponseEntity<List<Inventory>>(inventoryList, HttpStatus.OK);
    }

     @CrossOrigin(origins = "http://localhost:8100")
     @PostMapping(value={"/assign"})
     public ResponseEntity<Boolean> assignInventory(@RequestParam(name="id")Long distriubutorId) {
         return new ResponseEntity<Boolean>(inventoryService.assigneInventory(distriubutorId), HttpStatus.OK);
     }
    
     @CrossOrigin(origins = "http://localhost:8100")
     @PutMapping(value={"/update"})
     public ResponseEntity<Boolean> updateInventory(@RequestParam(name="id")Long inventoryId, @RequestParam(name="stock")Integer stock, @RequestParam(name="action")String action, @RequestParam(name="admin")Long adminId) {
         return new ResponseEntity<Boolean>(inventoryService.updateInventory(inventoryId, stock, action, adminId), HttpStatus.OK);
     }
     
     @CrossOrigin(origins = "http://localhost:8100")
     @GetMapping(value={"/transactions/all"})
     public ResponseEntity<List<InventoryTransaction>> getInventoryTransactions(@RequestParam(name="id") Long id) {
         List<InventoryTransaction> inventoryList = inventoryService.getAllInventoryTransactions(id);
         log.info(inventoryList);
         return new ResponseEntity<List<InventoryTransaction>>(inventoryList, HttpStatus.OK);
     }
      
}
