/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.dns.domain.Consumer
 *  com.dns.resource.ProductResource
 *  com.dns.service.ConsumerService
 *  org.apache.log4j.Logger
 *  org.springframework.beans.factory.annotation.Autowired
 *  org.springframework.http.HttpStatus
 *  org.springframework.http.ResponseEntity
 *  org.springframework.web.bind.annotation.GetMapping
 *  org.springframework.web.bind.annotation.PostMapping
 *  org.springframework.web.bind.annotation.RequestBody
 *  org.springframework.web.bind.annotation.RequestMapping
 *  org.springframework.web.bind.annotation.RequestParam
 *  org.springframework.web.bind.annotation.RestController
 */
package com.dns.resource;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dns.domain.Consumer;
import com.dns.domain.Distributor;
import com.dns.repository.DistributorRepository;
import com.dns.repository.ProductRepository;
import com.dns.service.ConsumerService;
import com.dns.service.DistributorService;
import com.dns.service.FilterService;
import com.dns.service.dto.ConsumerFilterDTO;
import com.dns.service.dto.ConsumerListAlongWithDistributorDTO;
import com.dns.service.dto.DistributorFilterDTO;
import com.dns.service.dto.DistributorFilterDetailResultDTO;
import com.dns.service.dto.FilterDetailsDTO;
import com.dns.service.dto.ProductFilterDTO;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@RestController
@RequestMapping(value={"/search"})
public class FilterResource {
    Logger log = Logger.getLogger(FilterResource.class);
    @Autowired
    private ConsumerService consumerService;
    @Autowired
    private DistributorRepository distributorRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private DistributorService distributorService;
    @Autowired
    private FilterService filterService;
    
    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/city"})
    public ResponseEntity<List<String>> GetCities() {
    	List<String> cities=null;
		try {
			cities = distributorRepository.getCities();
		} catch (Exception e) {
			log.error("Getting error while retrieving cities list",e );
		}
    	return new ResponseEntity<List<String>>(cities, HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/city_wise_consumer"})
    public ResponseEntity<ConsumerListAlongWithDistributorDTO> GetCityWiseDistributorAndConsumerList(@RequestParam(name="city") String city) {
    	List<Consumer> consumerList=null;
    	Distributor distributor=null;
		try {
			consumerList = consumerService.fetchConsumerListByCity(city);
		} catch (Exception e) {
			log.error("Getting error while retrieving cities list",e );
		}
		
		try {
			distributor = distributorService.getDistributorByCity(city);
		} catch (Exception e) {
			log.error("Getting error while fetching City wise DistriButor",e);
		}
    	return new ResponseEntity<ConsumerListAlongWithDistributorDTO>(new ConsumerListAlongWithDistributorDTO(consumerList, distributor), HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/product"})
    public ResponseEntity<List<ProductFilterDTO>> getProductFilterResults(@RequestBody FilterDetailsDTO filterBody) {
    	
    	return new ResponseEntity<List<ProductFilterDTO>>(filterService.getProductFilterResults(filterBody), HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/distributor"})
    public ResponseEntity<List<DistributorFilterDTO>> getDistributorFilterResults(@RequestBody FilterDetailsDTO filterBody) {
    	
    	return new ResponseEntity<List<DistributorFilterDTO>>(filterService.getDistributorFilterResults(filterBody), HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/consumer"})
    public ResponseEntity<List<ConsumerFilterDTO>> getConsumerFilterResults(@RequestBody FilterDetailsDTO filterBody) {
    	
    	return new ResponseEntity<List<ConsumerFilterDTO>>(filterService.getConsumerFilterResults(filterBody), HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/distributor/detail"})
    public ResponseEntity<List<DistributorFilterDetailResultDTO>> getDetailDistributorFilterResult(@RequestParam(name="id")Long id, @RequestParam(name="fromDate")String fromDate,  @RequestParam(name="toDate")String toDate) {
    	
    	return new ResponseEntity<List<DistributorFilterDetailResultDTO>>(distributorService.getDetailDistributorFilterResult(id, fromDate, toDate), HttpStatus.OK);
    }
}
