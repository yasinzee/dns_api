package com.dns.resource;

import com.dns.domain.Distributor;
import com.dns.domain.SaleRecord;
import com.dns.resource.ProductResource;
import com.dns.service.DistributorService;
import com.dns.service.OrderService;
import com.dns.service.dto.DistributorDTO;
import com.dns.service.dto.OrderDetailsDTO;
import com.dns.service.dto.OrderDetailsWithDisountedProductsDTO;
import java.math.BigDecimal;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
@RestController
@RequestMapping(value={"/distributor"})
public class DistributorResource {
    private static Logger log = Logger.getLogger(ProductResource.class);
    @Autowired
    private DistributorService distributorService;
    @Autowired
    private OrderService orderService;

    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/add"})
    public ResponseEntity<Boolean> addDistributor(@RequestBody DistributorDTO distributor) {
        log.info(distributor);
        return distributorService.storeDistributor(distributor);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/update"})
    public ResponseEntity<Boolean> updateDistributor(@RequestBody Distributor distributor) {
        log.info(distributor);
        return distributorService.updateDistributor(distributor);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/order"})
    public ResponseEntity<SaleRecord> insertOrders(@RequestBody OrderDetailsDTO orderDetails) {
        SaleRecord saleRecord = orderService.addOrders(orderDetails);
        return new ResponseEntity<SaleRecord>(saleRecord, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/orders/fetch/paid"})
    public ResponseEntity<List<SaleRecord>> fetchAllPaidOrders(@RequestParam(name="id") Long id) {
        List<SaleRecord> orderList = orderService.fetchAllPaidRecords(id);
        log.info(orderList);
        return new ResponseEntity<List<SaleRecord>>(orderList, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/orders/fetch/dues"})
    public ResponseEntity<List<SaleRecord>> fetchAllDuesOrders(@RequestParam(name="id") Long id) {
        List<SaleRecord> orderList = orderService.fetchAllDuesRecords(id);
        log.info(orderList);
        return new ResponseEntity<List<SaleRecord>>(orderList, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/orders/product"})
    public ResponseEntity<OrderDetailsWithDisountedProductsDTO> fetchProductsByOrder(@RequestParam(name="id") Long id) {
        OrderDetailsWithDisountedProductsDTO productList = orderService.fetchProductByOrder(id);
        log.info(productList);
        return new ResponseEntity<OrderDetailsWithDisountedProductsDTO>(productList, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/orders/settle/dues/{id}/{amount}"})
    public ResponseEntity<Boolean> settleDues(@PathVariable(name="id") Long id, @PathVariable(name="amount") String amount) {
        orderService.settleDues(id, BigDecimal.valueOf(Long.valueOf(amount)));
        return new ResponseEntity<Boolean>(true, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/orders/duesamount"})
    public ResponseEntity<BigDecimal> getDuesAmountByConsumer(@RequestParam(name="id") Long id) {
        BigDecimal amountDues = orderService.getDuesAmountByConsumer(id);
        log.info(amountDues);
        if (amountDues == null) {
            amountDues = BigDecimal.ZERO;
        }
        return new ResponseEntity<BigDecimal>(amountDues, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8100")
    @PostMapping(value={"/change/password"})
    public ResponseEntity<Boolean> changePassword(@RequestParam(name="id") Long id, @RequestParam(name="current") String currentPassword, @RequestParam(name="new") String newPassword) {
        return new ResponseEntity<Boolean>(distributorService.changePassword(id, currentPassword, newPassword), HttpStatus.OK);
    }
    
    @CrossOrigin(origins = "http://localhost:8100")
    @GetMapping(value={"/all"})
    public ResponseEntity<List<Distributor>> fetchallDistributor() {
        return new ResponseEntity<List<Distributor>>(distributorService.fetchAllDistributor(), HttpStatus.OK);
    }
}
